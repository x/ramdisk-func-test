A functional testing framework used for ramdisk-based deployment tools,
e.g. bareon (https://wiki.openstack.org/wiki/Bareon)

Provides an API to:
- create virtual nodes from template
- create virtual networks
- execute commands on the nodes
- transfer files to/from nodes
- cleanup resources
